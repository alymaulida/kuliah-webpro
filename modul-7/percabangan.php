<?php

$Nomor = 2;
$background = '#001d3d';
$color = '#a2d2ff';

switch($Nomor){
    case 1:
        echo "Merah";
        break;
    case 2:
        echo "Biru";
        break;
    case 3:
        echo "Hijau";
        break;
    case 4:
        echo "Kuning";
        break;
    default: 
        echo "Warna yang dipilih tidak ada";

}
?>

<style>

    body{
        background: <?php echo $background; ?>;
        color: <?php echo $color; ?>;
    }
</style>
