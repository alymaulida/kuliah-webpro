<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tubeswebpro";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, name, email, password FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "id: " . $row["id"]. "<br>";
    echo " - Name: " . $row["name"]. "<br>";
    echo " " . $row["email"]." <br>";
    echo " Password" .$row["password"]. "<br>";
    echo "<br>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>